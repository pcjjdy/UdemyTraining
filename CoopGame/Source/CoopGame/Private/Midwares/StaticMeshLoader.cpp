// Fill out your copyright notice in the Description page of Project Settings.

#include "StaticMeshLoader.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"


// Sets default values
AStaticMeshLoader::AStaticMeshLoader()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
// 	MeshURL = "StaticMesh'G:/Cube.uasset'";
	SuperMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SuperMesh"));
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));
	if (MeshAsset.Object != nullptr)
	{
		SuperMesh->SetStaticMesh(MeshAsset.Object);
	}
}

// Called when the game starts or when spawned
void AStaticMeshLoader::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStaticMeshLoader::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

